package service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import model.Training;
import model.TrainingDao;


@Service("trainingService")
public class TrainingService {
	
private TrainingDao trainingModel;
	
	@Autowired
	public void setTrainingDao(TrainingDao trainingModel) {
		this.trainingModel = trainingModel;
	}

	
	
	// - GROUP OF TRAININGS EXTRACTION
	
	public List<Training> getTrainigs(){		
		return trainingModel.getTrainings();		
	}		
	public Training getTodaysTraining(String userName, String weekDay){	
		return trainingModel.getTraining(userName, weekDay);		
	}
	public List<Training> getPublicOrExampleTraining(String userName) {
		return trainingModel.getPublicOrExampleTraining(userName);		
	}
	public List<Training> getUsersTrainings(String userName) {
		return trainingModel.getUsersTrainings(userName);
	}

	
	
	// - CHECK IF EXISTS

	public boolean trainingExists(String username, String trainingName) {
		return trainingModel.trainingExists(username, trainingName);		
	}
	public boolean trainingExists(String userName, String trainingName, String weekDay) {
		return trainingModel.trainingExists(userName, trainingName, weekDay);	
	}
	public boolean trainingForDayExists(String username, String weekDay) {
		return trainingModel.trainingForDayExists(username, weekDay);
	}
	
	
	
	// - SINGLE TRAINING PRESENTATION

	public Training presentTraining(String username, String trainingName) {
		return trainingModel.presentTraining(username, trainingName);		
	}


	
	
	// - CREATION AND UPDATE
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	public void createTraining(Training training) {		
		trainingModel.createTraining(training);		
	}
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	public boolean deleteTraining(String username, String trainingName) {
		return trainingModel.deleteTraining(username, trainingName);
	}
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	public void updateTraining(Training training, Training safeCopy) {
		trainingModel.updateTraining(training, safeCopy);
	}




	// - EXERCISES ADDITION AND EXTRACTION
	
	public void addExercisesName(List<String> userExercisesList, String userName ) {
		trainingModel.addExercisesName(userExercisesList, userName);		
	}
	public Map<String, List<String>> getUsersExercisesNames(String userName) {
		return trainingModel.getUsersExercisesNames(userName);		
	}
	public List<String> getUsersWeekDays(String userName) {
		return trainingModel.getUsersWeekDays(userName);
	}
	
	

	/////////////////////// Administration Methods //////////////////////////

	public void makeItPublic(String username, String trainingName) {
		trainingModel.makeItPublic(username, trainingName);
	}



	

	
	
	
//	public List<String> getAllExercisesNames(String userName) {
//	return trainingModel.getAllExercisesNames(userName);	
//}
	
	

//	public Map<String, List<String>> getAllPublicExercisesNamesII() {		
//		return trainingModel.getAllPublicExercisesNamesII();		
//	}


//	public void addExercisesNamesPack() {
//		trainingModel.addExercisesNamesPack();
//	}


	


	

}
