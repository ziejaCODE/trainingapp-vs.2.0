package controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import model.Exercise;
import model.Set;
import model.Training;
import model.User;
import service.TrainingService;

import org.json.*;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


@Controller
public class TrainingController {

	private TrainingService trainingService;

	// Save copy of training for safe keeping
	private Training safeCopy;
	private List<Training> trainings;
	private Map <String, List<String>> exerciseNames;	
	
	@Autowired
	public void setTrainingService(TrainingService trainingService) {
		this.trainingService = trainingService;
	}

	//*	
	// this method will direct user to todays training
	@RequestMapping("/showTodaysTraining")
	public String showTraining(Model model, Principal principal) {
		
		if(principal != null){
			User user = new User();
			user.setUserName(checkUserName(principal));
	
			SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); 
			String weekDay = simpleDateformat.format(new Date());
			
			Training training = new Training();
			training.setUserName(user);
			
			//System.out.println("weekDay " + weekDay + " user " + checkUserName(principal));
			
			if (trainingService.trainingForDayExists(checkUserName(principal), weekDay)) {
				
				System.out.println("weekDay " + weekDay + " user " + checkUserName(principal));
				
				training = trainingService.getTodaysTraining(checkUserName(principal), weekDay);			
				safeCopy = training;	
				model = modelBuilder(training, model);			
				
				System.out.println("Training name " + training.getTrainingName());
				
				return "showTraining";
			} else {			
				return "noTrainingToday";
			}
		}else{
			return "logIn";
		}
	}

	
	//*
	// This method will present list of all available public or examples training in application database
	@RequestMapping(value = "/showAllTrainings")
	public String showAllTrainings(Model model, Principal principal) {
		
		String userName = null;
		
		if (principal == null) {
			userName = "show";
		} else {			
			userName = "public";
		}
		
		trainings = trainingService.getPublicOrExampleTraining(userName);
		model.addAttribute("trainings", trainings);
		
		//System.out.println("List of Trainig");
		for(Training train: trainings){		
			System.out.println("- " + train.getTrainingName());
		}
		
		return "showAllTrainings";
	}
	

	
	//*
	// this method will present single example training for guest user
	@RequestMapping(value = "/presentExampleTraining", method = RequestMethod.GET)
	public String presentExampleTraining(Model model, String trainingName, Principal principal) {
		
		String username = null;
		String realUsername = null;

		if (principal == null) {
			username = "show";
		} else {
			realUsername = principal.getName();
			username = "public";
		}
		
		User user = new User();
		user.setUserName(username);

		User realUser = new User();
		realUser.setUserName(realUsername);
		
		Training training = trainingService.presentTraining(username, trainingName);		
		training.setUserName(user);
		
		safeCopy = training;	

		model = modelBuilder(training, model);
		model.addAttribute("realUser", realUser);
		
		return "showExampleTraining";
	}


	
	
	//*
	// this method will present list of all training's for particular user
	@RequestMapping("/showUsersTrainings")
	public String showUsersTrainings(Model model, Principal principal) {
		
		if(principal != null){
		safeCopy = null;		
		trainings = trainingService.getUsersTrainings(checkUserName(principal));
			if(trainings.size() <= 0){
				return "trainingDataBaseEmpty";
			}else{
				model.addAttribute("trainings", trainings);		
				System.out.println("List of Trainig");
				for(Training train: trainings){		
					System.out.println("- " + train.getTrainingName());
				}
			return "showUsersTrainings";
			}
		}else{
			return "logIn";
		}
	}
	
	
	
	//*
	// this method will present single training for use	
	@RequestMapping(value = "/presentTraining", method = RequestMethod.GET)
	public String presentTraining(Model model, String trainingName, Principal principal) {		
		
		if(principal != null){
			Training training = trainingBuilder(trainingName, principal);
			
			safeCopy = training;		
			
			model = modelBuilder(training, model );
			
			return "showTraining";
		}else{
			return "logIn";
		}
	}
	
	

	//*
	// method create Model object for sending 
	public Model modelBuilder(Training training, Model model){		
			
		// list of week days taken
		List<String> weekDaysTaken = new ArrayList<String>();
		
		
		
		//Map <String, List<String>> 
		exerciseNames = new HashMap<String, List<String>>(); 			
		exerciseNames = trainingService.getUsersExercisesNames(training.getUserName());		
		
		System.out.println("exerciseNames for 1 " + exerciseNames);
		
		weekDaysTaken = trainingService.getUsersWeekDays(training.getUserName());	
		
		 //Convert Training to  Json			
        String json = getConvertedString(training);		
		//Convert ExerciseNames to  Json	        
        String namesMap = getConvertedString(exerciseNames);        
        //Convert WeekDays to  Json	        
        String weekDaysList = getConvertedString(weekDaysTaken);
        
        model.addAttribute("weekDaysList", weekDaysList);
        model.addAttribute("namesMap", namesMap);
		model.addAttribute("training", training);
		model.addAttribute("json", json);
		
		return model;
	}
	
	
	
	//*
	// method creates Training Object from extracted data
	public Training trainingBuilder(String trainingName, Principal principal){
		
		User user = new User();
		user.setUserName(checkUserName(principal));

		Training  training = trainingService.presentTraining(checkUserName(principal), trainingName);
		training.setUserName(user);	
		
		return training;
	}

	
	
	// this method will update single training
	@RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
	public String doUpdate(@RequestBody Training training, Principal principal, HttpServletResponse response) {

		if(principal != null){
			User user = new User();
			user.setUserName(checkUserName(principal));
			training.setUserName(user);
				
			if( !(safeCopy.getTrainingName().equalsIgnoreCase(training.getTrainingName()))){
				trainingService.deleteTraining(checkUserName(principal), safeCopy.getTrainingName());
			}
			
			
			if (trainingService.trainingExists(training.getUserName(), training.getTrainingName()) )  {
					trainingService.updateTraining(training, safeCopy);			
			} else {
				trainingService.createTraining(training);
			}
			
			safeCopy = null;
				
			return "showUsersTrainings";
		}else{
			return "logIn";
		}
		
		
	}

	
	
	
	
	
	// this method will add single exercise name training	
	@RequestMapping(value = "/getNewExercise", method = RequestMethod.POST)
	public String addExercisesName(@RequestBody String userExercises, Principal principal) {
	
		//String userExercises = userExer;		
		
		//System.out.println("tutaj jestem " + userExercises);// test
		
		//System.out.println("exerciseNames for 2 " + exerciseNames.get("USERS EXERCISES"));
		if(principal != null){	
			if(!(userExercises.equalsIgnoreCase("%5B%5D="))){
			
				List<String> userExercisesList = null;
				
				String regx = "%5B%22";
				userExercises = userExercises.replace(regx, "[\"");
				regx = "%22%5D=";
				userExercises = userExercises.replace(regx, "\"]");
				regx = "%22%2C%22";
				while(userExercises.contains(regx)){
					userExercises = userExercises.replace(regx, "\",\"");
				}
				regx = "+";
				while(userExercises.contains(regx)){
					userExercises = userExercises.replace(regx, " ");
				}
				
				
				int b = exerciseNames.get("USERS EXERCISES").size();
				
				//System.out.println("use from " + b + " to " + a);
				
				JSONParser parser = new JSONParser();	
				List<String> shortList = new ArrayList<String>();
				Object  usersExe = null;		   
			    try{	    	
			    	usersExe =  parser.parse(userExercises);
			    	//System.out.println("What character " + usersExe);
			    	userExercisesList = (List)usersExe;
			    	//System.out.println("use from " + b + " to " + userExercisesList.size());
			    	
			    	for(int i = b; i < userExercisesList.size(); i++){
			    		
			    		shortList.add(userExercisesList.get(i));
			    		//System.out.println(" i = " + userExercisesList.get(i));
			    	}
			    	
			    	//System.out.println("after all this hassel " + shortList);
			    	trainingService.addExercisesName(shortList, checkUserName(principal));
			    	
			      }catch(ParseException pe){	    	  
			         System.out.println("Error "  + userExercises);
			         pe.printStackTrace();
			      }
				return "showTraining";
			}else{
				System.out.println("No need for updating exercises");
				return "showTraining";
			}
			
		}else{
		return "logIn";
	}
		
	}
	

	
	
	//**
	// this method will convert data to json format
	public String getConvertedString(Object objectToConvert){
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();		
		
		gson = new GsonBuilder().setPrettyPrinting().create();//
		String jsonString = "";
        try {
        	jsonString = gson.toJson (objectToConvert);
		} catch (Exception e1) {
			e1.printStackTrace();
		}		
		return jsonString;		
	}
	
	
	
	
	//**
	// this method will create single training for use
	@RequestMapping("/createTraining")
	public String createTraining(Model model, Principal principal) {

		
		if(principal != null){
			User user = new User();
			String userName = checkUserName(principal);
			user.setUserName(userName);
			
			List<Training> trainings = trainingService.getUsersTrainings(userName);		
			List<String> userListOfTrainings = new ArrayList<>();		
			List<String> weekDaysTaken = new ArrayList<String>();
			
			weekDaysTaken = trainingService.getUsersWeekDays(userName);	
			
			for(Training t1: trainings){
				userListOfTrainings.add(t1.getTrainingName());			
			}
				
			exerciseNames = new HashMap<String, List<String>>();			
			exerciseNames = trainingService.getUsersExercisesNames(checkUserName(principal));		
				       
			Gson gson = new GsonBuilder().setPrettyPrinting().create();	        
			String namesMap = "";
		       
			try {				
				namesMap = gson.toJson ( exerciseNames );			
			} catch (Exception e1) {				
				e1.printStackTrace();			
			}
			
			String weekDaysList = getConvertedString(weekDaysTaken);
		    System.out.println("weekDaysList " + weekDaysList);  
			
		    model.addAttribute("weekDaysList", weekDaysList);		
			model.addAttribute("namesMap", namesMap);			
			model.addAttribute("user", user);			
			model.addAttribute("userListOfTrainings", userListOfTrainings);
			
			return "createTraining";
		}else{
			return "logIn";
		}
		
	}
	
	
	@RequestMapping(value = "/doCreate", method = RequestMethod.POST)
	public String doCreate(@RequestBody Training training, Principal principal, HttpServletResponse response) {
		
		if(principal != null){
			safeCopy = null;
			
			User user = new User();
			user.setUserName(principal.getName());
			training.setUserName(user);			
			
			if (trainingService.trainingExists(training.getUserName(), training.getTrainingName()) && (!(safeCopy.getUserName().equalsIgnoreCase("public")))){			
				trainingService.updateTraining(training, safeCopy);	
				
			}else if (!(trainingService.trainingExists(training.getUserName(), training.getTrainingName()))  && (safeCopy == null))  {			
				
				trainingService.createTraining(training);			
			
			}else{			
				
			}		
			return "showUsersTrainings";
		}else{
			return "logIn";
		}
	}
	

	
	//**
	// this method will delete single training		
	@RequestMapping(value = "/deleteTraining", method = RequestMethod.GET)		
	public String deleteTraining(String trainingName, String weekDay, Principal principal) {			
	
		if(principal != null){
			String username = principal.getName();			
			trainingService.deleteTraining(username, trainingName);			
			return "trainingDeleted";
		}else{
			return "logIn";
		}
	}
	
	//**
	// this method will push training plan to public repository		
	@RequestMapping(value = "/changeTrainingToPublic", method = RequestMethod.GET)
	public String makeItPublic( String trainingName , Principal principal){
		
		if(principal != null){	
			trainingService.makeItPublic(checkUserName(principal), trainingName);
			return "trainingMadePublic";
		}else{
			return "logIn";
		}
	}
	
	//**
	// this method will check weather true user is logged in	
	public String checkUserName(Principal principal){		
		
		String username;
		try {
			username = principal.getName();
			return username;		
		} catch (Exception e1) {
			e1.printStackTrace();
			return "logIn";
		}
	}	
	
	
	@RequestMapping(value = "/justReload", method = RequestMethod.GET)
	public String justReload() {		
		//System.out.println("Reloading now ");// test		
		return "trainingUpdated";
	}
	
	
}
