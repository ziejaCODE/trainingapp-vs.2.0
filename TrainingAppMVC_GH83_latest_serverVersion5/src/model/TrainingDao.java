package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import controllers.TrainingRowMapper;

@Component("trainingDao")
public class TrainingDao {

	
	// - FIELD SETTING 
	
	private NamedParameterJdbcTemplate database1;
	
	// data source is set using JDBC
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.database1 = new NamedParameterJdbcTemplate(dataSource);		
	}

		
	public TrainingDao() {}

	

	
	// - SINGLE TRAINING PRESENTATION
	
	// this method will return one training from database for today day
	//@Transactional
	public Training getTraining(String userName, String weekDay) {

		// get training name
		MapSqlParameterSource parameter1 = new MapSqlParameterSource();
		parameter1.addValue("userName", userName);
		parameter1.addValue("weekDay", weekDay);

		//System.out.println("weekDay " + weekDay + " user " + userName);
		
		Training training = database1.queryForObject(
				"select * from training, users where training.userName=:userName"
						+ " and users.userName=:userName and training.weekDay = :weekDay and users.enabled=true",
				parameter1, new TrainingRowMapper());

		String trainingName = training.getTrainingName();
		
		System.out.println("Training name " + trainingName);
		
		parameter1.addValue("trainingName", trainingName);

		System.out.println("training name is " + trainingName); // test

		// get all exercises from that training
		List<Exercise> exercises = database1.query(
				"select * from" + " trainingSchedule where trainingName=:trainingName", parameter1,
				new RowMapper<Exercise>() {

					public Exercise mapRow(ResultSet rs, int rowNum) throws SQLException {

						Exercise exercise = new Exercise();
						exercise.setNameOfExercise(rs.getString("exerciseName"));
						exercise.setGroupId(rs.getInt("groupId"));
						int groupId = exercise.getGroupId();

						MapSqlParameterSource parameter = new MapSqlParameterSource();
						parameter.addValue("groupId", groupId);
						System.out.println("Group id is: " + groupId);
						// get all sets from each exercise
						List<Set> sets = database1.query(
								"select  setNo, repsNo, weight from setPlan where groupId=:groupId", parameter,
								new RowMapper<Set>() {

									public Set mapRow(ResultSet rs, int rowNum) throws SQLException {
										Set set = new Set();
										set.setSetNo(rs.getInt("setNo"));
										set.setReps(rs.getInt("repsNo"));
										set.setWeight(rs.getDouble("weight"));
										return set;
									}
								});
						exercise.setSets(sets);
						return exercise;
					}
				});
		training.setExercises(exercises);
		return training;
	}

	// this method will present full training to the user
	@Transactional
	public Training presentTraining(String username, String trainingName) {

		System.out.println("requested trainig is " + trainingName + " username " + username);

		// Training training = new Training();

		MapSqlParameterSource parameter1 = new MapSqlParameterSource();
		parameter1.addValue("trainingName", trainingName);
		parameter1.addValue("userName", username);

		System.out.println("1 requested trainig is " + trainingName + " username " + username);

		Training training = null;
		try {
			training = database1.queryForObject(

					"select * from training, users where training.userName=:userName"
							+ " and users.userName=:userName and training.trainingName = :trainingName and users.enabled=true",

					parameter1, new TrainingRowMapper());
		} catch (DataAccessException e) {
			System.out.println("traning not extracted " + training);
			e.printStackTrace();
		}

		
		
		
		
		// get all exercises from that training
		List<Exercise> exercises = null;
		try {
			exercises = database1.query(
					"select * from" + " trainingSchedule where trainingName=:trainingName", parameter1,
					new RowMapper<Exercise>() {

						public Exercise mapRow(ResultSet rs, int rowNum) throws SQLException {
							Exercise exercise = new Exercise();
							exercise.setNameOfExercise(rs.getString("exerciseName"));
							exercise.setGroupId(rs.getInt("groupId"));
							int groupId = exercise.getGroupId();

							MapSqlParameterSource parameter = new MapSqlParameterSource();
							parameter.addValue("groupId", groupId);
							System.out.println("Group id is: " + groupId);
							// get all sets from each exercise
							List<Set> sets = database1.query(
									"select setId, setNo, repsNo, weight from setPlan where groupId=:groupId", parameter,
									new RowMapper<Set>() {

										public Set mapRow(ResultSet rs, int rowNum) throws SQLException {

											System.out.println("setId is " + rs.getInt("setId"));

											Set set = new Set();
											set.setSetNo(rs.getInt("setNo"));
											set.setReps(rs.getInt("repsNo"));
											set.setWeight(rs.getDouble("weight"));
											return set;
										}
									});
							exercise.setSets(sets);
							return exercise;
						}
					});
		} catch (DataAccessException e) {
			System.out.println("Rest of traning not extracted " + training);
			e.printStackTrace();
		}

		training.setTrainingName(trainingName);
		// training.setWeekDay(weekDay);

		System.out.println("2 requested trainig is " + trainingName + "username " + username);

		training.setExercises(exercises);
		return training;

	}

	
	
	
	// - GROUP OF TRAININGS EXTRACTION
	
	// this method will return all training's for particular user
	@Transactional
	public List<Training> getUsersTrainings(String userName) {

		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("userName", userName);

		//System.out.println("DaohowUsersTrainings on place");

		return database1.query("select * from training, users where training.userName=:userName"
				+ " and users.userName=:userName and users.enabled=true", parameter, new TrainingRowMapper());

	}

	// this method will return all training's from database -------- ADMIN USE
	public List<Training> getTrainings() {

		return database1.query(
				"select * from training, users where training.username=users.username and users.enabled=true",
				new TrainingRowMapper());

	}
	
	// this method will display all public training available for registered users
	public List<Training> getPublicOrExampleTraining(String userName) {

		return database1.query("select * from training where username ='" + userName + "'", new RowMapper<Training>() {
			public Training mapRow(ResultSet rs, int rowNum) throws SQLException {
				Training training = new Training();
				training.setTrainingName(rs.getString("trainingName"));
				return training;
			}
		});
	}

	
	
	
	// - CHECK IF EXISTS
	
	// method that checks if training exists I
	public boolean trainingExists(String userName, String trainingName) {

		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("userName", userName);
		parameter.addValue("trainingName", trainingName);

		// System.out.println("Exist check1 Username " + userName + "
		// trainingName " + trainingName); // to
		// remove

		return database1.queryForObject(
				"select count(*) from training where userName=:userName and trainingName=:trainingName", parameter,
				Integer.class) > 0;
	}

	// method that checks if training exists II
	public boolean trainingExists(String userName, String trainingName, String weekDay) {

		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("userName", userName);
		parameter.addValue("trainingName", trainingName);
		parameter.addValue("weekDay", weekDay);

		// System.out.println("Exist check1 Username " + userName + "
		// trainingName " + trainingName + " weekday " + weekDay); // to
		// remove

		return database1.queryForObject(
				"select count(*) from training where userName=:userName and trainingName=:trainingName and weekDay=:weekDay",
				parameter, Integer.class) > 0;
	}

	// method that checks if training for particular day exists
	public boolean trainingForDayExists(String userName, String weekDay) {

		// it will return list of trainings for particular day
		
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("userName", userName);
		parameter.addValue("weekDay", weekDay);

		System.out.println("weekDay " + weekDay + " user " + userName);
		return database1.queryForObject("select count(*) from training where userName=:userName and weekDay=:weekDay",
				parameter, Integer.class) > 0;
				
	}

	
	
	
	// - CREATION AND UPDATE
	
	// this method will create new user
	@Transactional
	public boolean createTraining(Training training) {

		// System.out.println("Create test from dao " +
		// training.getTrainingName() + ", " + training.getWeekDay() + ", " +
		// training.getUserName() + ", " + training.toString());
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		// parameter.addValue("trainingName", training.getTrainingName() + "_" +
		// training.getUserName());
		parameter.addValue("trainingName", training.getTrainingName());
		parameter.addValue("weekDay", training.getWeekDay());
		parameter.addValue("userName", training.getUserName());

//		System.out.println();
//		System.out.println("test from dao " + training.getTrainingName() + ", " + training.getWeekDay() + ", "
//				+ training.getUserName() + ", " + training.toString());
//		// -

		try {

			// insert new training into database
			database1.update(
					"insert into training (trainingName, weekDay, userName) values (:trainingName, :weekDay , :userName)",
					parameter);
//			System.out.println("training table created");

			List<Exercise> exercises = training.getExercises();

			int n = 1;

//			System.out.println("Wszystkie cwiczenia is " + exercises.size());
			// System.out.println("Wszystkie cwiczenia" + exercises.toString());

			for (int i = 0; i < exercises.size(); i++) {

				parameter.addValue("exerciseName", exercises.get(i).getNameOfExercise());

				// System.out.println("Dane cwiczenie to" +
				// exercises.get(i).getNameOfExercise());

				database1.update(
						"insert into trainingSchedule (trainingName, exerciseName) values  (:trainingName, :exerciseName)",
						parameter);

				// Integer groupId = database1.queryForObject( "select groupId
				// from trainingSchedule where trainingName=:trainingName and
				// exerciseName=:exerciseName ", parameter, Integer.class);

				// System.out.println(" this Group id is: " + groupId);// test
				// to
				// remove

				Integer groupId = null;

				List<Integer> strLst = database1.query(
						"select groupId from trainingSchedule where trainingName=:trainingName and exerciseName=:exerciseName ",
						parameter, new RowMapper<Integer>() {

							public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
								return rs.getInt(1);
							}
						});

				if (strLst.isEmpty()) {
					System.out.println(" this Group id is null: " + groupId);// test
																				// to
																				// remove

				} else if (strLst.size() == 1) { // list contains exactly 1
													// element
					groupId = strLst.get(0);

					// // WORKING //
					// List<Set> sets = exercises.get(i).getSets();
					//
					// System.out.println("Wszystkie set to " + sets.size());
					//
					// for (int j = 0; j < sets.size(); j++) {
					//
					// Set set = sets.get(j);
					// System.out.println("groupId is " + groupId);
					//
					// parameter.addValue("groupId", groupId);
					//
					// System.out.println("Set no is " + set.getSetNo());
					// parameter.addValue("setNo", set.getSetNo());
					//
					// parameter.addValue("repsNo", set.getReps());
					// System.out.println("repsNo is " + set.getReps());
					//
					// parameter.addValue("weight", set.getWeight());
					// System.out.println("weight is " + set.getWeight());
					//
					// database1.update("insert setPlan (groupId, setNo, repsNo,
					// weight) values (:groupId, :setNo, :repsNo, :weight)",
					// parameter);
					//
					// Integer setId = database1.queryForObject(
					// "select setId from setPlan where groupId=:groupId and
					// setNo=:setNo ", parameter, Integer.class);
					// parameter.addValue("setId", setId);
					// System.out.println("setId is " + setId);
					//
					// database1.update("insert sets (setId, groupId) values
					// (:setId, :groupId)", parameter);
					// }

				} else {
//					System.out.println(" list contains more than 1 elements " + strLst.size());// test
																								// to
																								// remove

					groupId = strLst.get(0 + n);
					n++;
				}

				// -

				// WORKING //
				List<Set> sets = exercises.get(i).getSets();

//				System.out.println("Wszystkie set to " + sets.size());

				for (int j = 0; j < sets.size(); j++) {

					Set set = sets.get(j);
//					System.out.println("groupId is " + groupId);

					parameter.addValue("groupId", groupId);

//					System.out.println("Set no is " + set.getSetNo());
					parameter.addValue("setNo", set.getSetNo());

					parameter.addValue("repsNo", set.getReps());
//					System.out.println("repsNo is " + set.getReps());

					parameter.addValue("weight", set.getWeight());
//					System.out.println("weight is " + set.getWeight());

					database1.update(
							"insert setPlan (groupId, setNo, repsNo, weight) values  (:groupId, :setNo, :repsNo, :weight)",
							parameter);

					Integer setId = database1.queryForObject(
							"select setId from setPlan where groupId=:groupId and setNo=:setNo ", parameter,
							Integer.class);
					parameter.addValue("setId", setId);
//					System.out.println("setId is " + setId);

					database1.update("insert sets (setId, groupId) values  (:setId, :groupId)", parameter);
				}
			}

			System.out.println("Training Saved");
			return true;

		} catch (DataAccessException e) {

			System.out.println("create not possible something is missing");
			e.printStackTrace();
			return false;
		}

	}

	// this method will delete full training in all tables
	@Transactional
	public boolean deleteTraining(String userName, String trainingName) {

		if (trainingExists(userName, trainingName)) {

			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter.addValue("trainingName", trainingName);
			// parameter.addValue("weekDay", weekDay);
			parameter.addValue("userName", userName);

			// delete all from training table
			database1.update("delete from training where trainingName=:trainingName and userName=:userName ",
					parameter);
			//System.out.println("t Deleted");

			// get all related groupId
			final List<Integer> groupIds = new ArrayList<>();

			database1.query("select * from" + " trainingSchedule where trainingName=:trainingName", parameter,
					new RowMapper<Exercise>() {

						public Exercise mapRow(ResultSet rs, int rowNum) throws SQLException {
							Exercise exercise = new Exercise();
							exercise.setNameOfExercise(rs.getString("exerciseName"));
							exercise.setGroupId(rs.getInt("groupId"));
							int groupId = exercise.getGroupId();
							System.out.println("Group id is " + groupId);
							groupIds.add(groupId);
							return exercise;
						}
					});
			//System.out.println("Group ids are " + groupIds.toString());

			// delete all from trainingSchedule table
			for (int i = 0; i < groupIds.size(); i++) {

				try {
					parameter.addValue("groupId", groupIds.get(i));
					database1.update("delete from trainingSchedule where groupId=:groupId", parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
				try {
					database1.update("delete from setPlan where groupId=:groupId", parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
				try {
					database1.update("delete from sets where groupId=:groupId", parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
			}
			//System.out.println("training deleted");
			//System.out.println();
			return true;
		} else {
			//System.out.println("training is still there");
			//System.out.println();
			return false;
		}
	}

	// this method will create new user
	@Transactional
	public void updateTraining(Training training, Training safeCopy) {

		// save old training

		// get new training
		System.out.println();
		System.out.println("Update test from dao " + training.getTrainingName() + ", " + training.getWeekDay() + ", "
				+ training.getUserName());
		System.out.println();
		// System.out.println("Update test from dao " +
		// safeCopy.getTrainingName() + ", " + safeCopy.getWeekDay() + ", " +
		// safeCopy.getUserName());
		System.out.println();

		// if training exists can be updated
		if (trainingExists(safeCopy.getUserName(), safeCopy.getTrainingName(), safeCopy.getWeekDay())) {

			// System.out.println("Old training to delete exists " +
			// safeCopy.getTrainingName() + ", " + safeCopy.getWeekDay() + ", "
			// + safeCopy.getUserName());

			// delete old from database if training is not public
			if (!(safeCopy.getUserName().equalsIgnoreCase("public"))) {
				deleteTraining(safeCopy.getUserName(), safeCopy.getTrainingName());
				System.out.println("Safe copy with name " + safeCopy.getTrainingName() + " deleted");
			} else {
				System.out.println("Public cannot be deleted");
			}

			// System.out.println("Create test from dao " +
			// safeCopy.getTrainingName() + ", " + safeCopy.getWeekDay() + ", "
			// + safeCopy.getUserName() + ", " + safeCopy.toString());
			MapSqlParameterSource safeCopyParameter = new MapSqlParameterSource();
			safeCopyParameter.addValue("trainingName", safeCopy.getTrainingName());
			safeCopyParameter.addValue("weekDay", safeCopy.getWeekDay());
			safeCopyParameter.addValue("userName", safeCopy.getUserName());

			// System.out.println("Create test from dao " +
			// training.getTrainingName() + ", " + training.getWeekDay() + ", "
			// + training.getUserName() + ", " + training.toString());
			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter.addValue("trainingName", training.getTrainingName());
			parameter.addValue("weekDay", training.getWeekDay());
			parameter.addValue("userName", training.getUserName());

			// System.out.println();
			// System.out.println("test from dao " + training.getTrainingName()
			// + ", " + training.getWeekDay() + ", "
			// + training.getUserName() + ", " + training.toString());

			// -

			try {

				database1.update(
						"insert into training (trainingName, weekDay, userName) values (:trainingName, :weekDay , :userName)",
						parameter);
				System.out.println("training table updated");
			} catch (DataAccessException e) {
				e.printStackTrace();
			}

			List<Exercise> exercises = training.getExercises();

			// System.out.println("Wszystkie cwiczenia is " + exercises.size());
			// System.out.println("Wszystkie cwiczenia" + exercises.toString());

			int n = 1;

			for (int i = 0; i < exercises.size(); i++) {

				parameter.addValue("exerciseName", exercises.get(i).getNameOfExercise());

				// System.out.println("Dane cwiczenie to" +
				// exercises.get(i).getNameOfExercise());

				database1.update(
						"insert into trainingSchedule (trainingName, exerciseName) values  (:trainingName, :exerciseName)",
						parameter);

				Integer groupId = null;

				// try {
				// groupId = database1.queryForObject( "select groupId from
				// trainingSchedule where trainingName=:trainingName and
				// exerciseName=:exerciseName ", parameter, Integer.class);
				// } catch (DataAccessException e) {
				// System.out.println("\n Extracting Group id failed: " +
				// groupId);// test to remove
				// System.out.println();
				// e.printStackTrace();
				// }

				List<Integer> strLst = database1.query(
						"select groupId from trainingSchedule where trainingName=:trainingName and exerciseName=:exerciseName ",
						parameter, new RowMapper<Integer>() {

							public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
								return rs.getInt(1);
							}

						});

				if (strLst.isEmpty()) {
					System.out.println(" this Group id is null: " + groupId);// test
																				// to
																				// remove

				} else if (strLst.size() == 1) { // list contains exactly 1
													// element
					groupId = strLst.get(0);

					// // WORKING //
					// List<Set> sets = exercises.get(i).getSets();
					//
					// System.out.println("Wszystkie set to " + sets.size());
					//
					// for (int j = 0; j < sets.size(); j++) {
					//
					// Set set = sets.get(j);
					// System.out.println("groupId is " + groupId);
					//
					// parameter.addValue("groupId", groupId);
					//
					// System.out.println("Set no is " + set.getSetNo());
					// parameter.addValue("setNo", set.getSetNo());
					//
					// parameter.addValue("repsNo", set.getReps());
					// System.out.println("repsNo is " + set.getReps());
					//
					// parameter.addValue("weight", set.getWeight());
					// System.out.println("weight is " + set.getWeight());
					//
					// database1.update("insert setPlan (groupId, setNo, repsNo,
					// weight) values (:groupId, :setNo, :repsNo, :weight)",
					// parameter);
					//
					// Integer setId = database1.queryForObject(
					// "select setId from setPlan where groupId=:groupId and
					// setNo=:setNo ", parameter, Integer.class);
					// parameter.addValue("setId", setId);
					// System.out.println("setId is " + setId);
					//
					// database1.update("insert sets (setId, groupId) values
					// (:setId, :groupId)", parameter);
					// }
				} else {
					System.out.println(" list contains more than 1 elements " + strLst.size());// test
																								// to
																								// remove

					groupId = strLst.get(0 + n);
					n++;
				}

				// WORKING //
				List<Set> sets = exercises.get(i).getSets();

				System.out.println("Wszystkie set to " + sets.size());

				for (int j = 0; j < sets.size(); j++) {

					Set set = sets.get(j);
					System.out.println("groupId is " + groupId);

					parameter.addValue("groupId", groupId);

					System.out.println("Set no is " + set.getSetNo());
					parameter.addValue("setNo", set.getSetNo());

					parameter.addValue("repsNo", set.getReps());
					System.out.println("repsNo is " + set.getReps());

					parameter.addValue("weight", set.getWeight());
					System.out.println("weight is " + set.getWeight());

					database1.update(
							"insert setPlan (groupId, setNo, repsNo, weight) values  (:groupId, :setNo, :repsNo, :weight)",
							parameter);

					Integer setId = database1.queryForObject(
							"select setId from setPlan where groupId=:groupId and setNo=:setNo ", parameter,
							Integer.class);
					parameter.addValue("setId", setId);
					System.out.println("setId is " + setId);

					database1.update("insert sets (setId, groupId) values  (:setId, :groupId)", parameter);

				}

			}
			System.out.println("Training Saved");

			System.out.println("training updated");
		} else {
			System.out.println("training not updated");
		}

	}

	
	
	
	// - EXERCISES ADDITION AND EXTRACTION
	
	// exercise list extraction
	@Transactional
	public Map<String, List<String>> getUsersExercisesNames(String userName) {

		Map<String, List<String>> exerciseNamesMap = getAllPublicExercisesNames();

		String queryString = "SELECT exerciseName from excerciseList  where excerciseList.userName='" + userName + "'";

		List<String> exerciseNames = database1.query(queryString, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				String exerciseName = rs.getString("exerciseName");
				return exerciseName;
			}
		});

		//if (exerciseNames.size() > 0) {
			exerciseNamesMap.put("USERS EXERCISES", exerciseNames);
		//}
		return exerciseNamesMap;
	}

	
	@Transactional
	public Map<String, List<String>> getAllPublicExercisesNames() {

		Map<String, List<String>> exerciseNamesMap = new HashMap<String, List<String>>();

		List<String> bodyPartsNames = database1.query("select bodyPart from excerciseListUpdated",
				new RowMapper<String>() {
					public String mapRow(ResultSet rs, int rowNum) throws SQLException {
						String bodyPart = rs.getString("bodyPart");
						return bodyPart;
					}
				});

		HashSet<String> bdpSet = new HashSet<String>();
		bdpSet.addAll(bodyPartsNames);

		for (String bodyPart : bdpSet) {

			String queryString = "SELECT nameOfExercise from excerciseListUpdated where excerciseListUpdated.bodyPart='"
					+ bodyPart + "'";

			List<String> exerciseNames = database1.query(queryString, new RowMapper<String>() {
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					String nameOfExercise = rs.getString("nameOfExercise");
					return nameOfExercise;
				}
			});
			exerciseNamesMap.put(bodyPart, exerciseNames);
		}
		return exerciseNamesMap;
	}
	
	
	
	public void addExercisesName(List<String> userExercisesList, String userName) {
		//System.out.println("Add " + userExercisesList + " to database");
		for(String exeName: userExercisesList){		
			//System.out.println("Exercise name is - " + exeName);
			
			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter.addValue("userName", userName);
			parameter.addValue("exerciseName", exeName);
	
			try {
				database1.update("insert into excerciseList (userName, exerciseName) values (:userName, :exerciseName)", parameter);
				//System.out.println(exeName + " loaded");
			} catch (DataAccessException e) {
				//System.out.println("load for " + exeName + " not possible something is missing");
				e.printStackTrace();
			}	
			//System.out.println("Exercise name added result is - ");
		}		
		//System.out.println("All added");
	}

	
	
	
	
	
	
	/////////////////////// Administration Methods //////////////////////////
	
	
	// for administration use  
	public List<String> getAllExercisesNames(String userName) {

			if (!userName.equalsIgnoreCase("lolo")) {

				String queryString = "SELECT exerciseName from excerciseList  where excerciseList.userName='" + userName
						+ "'";

				List<String> exerciseNames1 = new ArrayList<>();

				// exerciseNames1.add("Select exercise");

				exerciseNames1 = database1.query(queryString, new RowMapper<String>() {
					public String mapRow(ResultSet rs, int rowNum) throws SQLException {
						String exerciseName = rs.getString("exerciseName");
						return exerciseName;
					}
				});

				queryString = "SELECT exerciseName from excerciseList  where excerciseList.userName='mira'";

				List<String> exerciseNames2 = database1.query(queryString, new RowMapper<String>() {
					public String mapRow(ResultSet rs, int rowNum) throws SQLException {
						String exerciseName = rs.getString("exerciseName");
						return exerciseName;
					}
				});
				System.out.println("exerciseNames " + exerciseNames1.toString());

				exerciseNames1.addAll(exerciseNames2);
				System.out.println("exerciseNames po dodaniu" + exerciseNames1.toString());
				return exerciseNames1;

			} else {
				List<String> exerciseNames = database1.query("select * from excerciseList ", new RowMapper<String>() {
					public String mapRow(ResultSet rs, int rowNum) throws SQLException {
						String nameOfExercise = rs.getString("nameOfExercise");
						return nameOfExercise;
					}
				});
				// System.out.println("exerciseNames " + exerciseNames.toString());
				return exerciseNames;
			}
		}

	
	
	
	public void makeItPublic(String userName, String trainingName) {

		if (trainingExists(userName, trainingName)) {

			String newTrainingName = trainingName.substring(0, trainingName.indexOf('_'));

			System.out.println("newTrainingName is " + newTrainingName);

			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter.addValue("trainingName", trainingName);
			parameter.addValue("newTrainingName", newTrainingName);
			parameter.addValue("userName", userName);

			database1.update(
					"update training set trainingName=:newTrainingName , userName='public' where trainingName=:trainingName and userName=:userName ",
					parameter);

			final List<Integer> groupIds = new ArrayList<>();

			database1.query("select * from" + " trainingSchedule where trainingName=:trainingName", parameter,
					new RowMapper<Exercise>() {

						public Exercise mapRow(ResultSet rs, int rowNum) throws SQLException {

							Exercise exercise = new Exercise();
							exercise.setNameOfExercise(rs.getString("exerciseName"));
							exercise.setGroupId(rs.getInt("groupId"));
							int groupId = exercise.getGroupId();
							System.out.println("Group id is " + groupId);
							groupIds.add(groupId);
							return exercise;
						}
					});
			System.out.println("Group ids are " + groupIds.toString());

			// delete all from trainingSchedule table
			for (int i = 0; i < groupIds.size(); i++) {

				try {
					parameter.addValue("groupId", groupIds.get(i));
					database1.update("update trainingSchedule set trainingName=:newTrainingName where groupId=:groupId",
							parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
				try {
					database1.update("update trainingSchedule set trainingName=:newTrainingName where groupId=:groupId",
							parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
				try {
					database1.update("update trainingSchedule set trainingName=:newTrainingName where groupId=:groupId",
							parameter);
				} catch (DataAccessException e) {
					e.printStackTrace();
				}
			}
			System.out.println("training Changed");
			System.out.println();

			System.out.println("Training has been changed");
		} else {
			System.out.println("Training has not been changed");
		}
	}


	public List<String> getUsersWeekDays(String userName) {			
		
		String queryString = "SELECT weekDay from training  where training.userName='" + userName + "'";

		 List<String> weekDayList = database1.query(queryString, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				String weekDay = rs.getString("weekDay");
				return weekDay;
			}
		});
		System.out.println("weekDay " + weekDayList.toString());
		 return weekDayList;
	}
	
	
}

