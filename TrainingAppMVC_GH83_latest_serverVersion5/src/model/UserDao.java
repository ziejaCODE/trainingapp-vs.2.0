package model;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("userDao")
public class UserDao {
	
	private NamedParameterJdbcTemplate database1;
	
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	public UserDao() {
		//System.out.println("Dao loaded");		
	}
	
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.database1 = new NamedParameterJdbcTemplate(dataSource);
		
	}
	
	
    // this method  will return all users from database only for admin access
	public List<User> getAllUsers(){		
		return database1.query("select * from users  ", BeanPropertyRowMapper.newInstance(User.class));		
	}
	
	
	// check if user exist using userName
	@Transactional
	public boolean userExists(String userName){
		
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("userName", userName);

		return database1.queryForObject("select count(*) from users where userName=:userName" ,
				parameter, Integer.class) > 0;
		
	}
	

	// this method  will create new user account
	@Transactional		
	public boolean createUser(User user){
				
		if(!(userExists(user.getUserName()))){
			
			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter.addValue("userName", user.getUserName());
			parameter.addValue("password", passwordEncoder.encode(user.getPassword()));
			parameter.addValue("userEmail", user.getUserEmail());
			parameter.addValue("userEmail", user.getUserEmail());
			parameter.addValue("authority", user.getAuthority());
			parameter.addValue("enabled", user.isEnabled());
			
			database1.update("insert into users (userName, userEmail, password, authority, enabled) values (:userName, :userEmail, :password, :authority ,:enabled)", parameter);		
			return database1.update("insert into authorities (userName, authority) values (:userName, :authority)", parameter) == 1;
		}else{
			return false;		
		}
	}
	
		// this method  will delete user account	
		@Transactional		
		public boolean deleteUser(String userName){			
			
			
			if(userExists(userName)){				
				MapSqlParameterSource parameter = new MapSqlParameterSource();
				parameter.addValue("userName", userName);			
				
				// delete all from training table
				int result1 = database1.update("delete from users where userName=:userName", parameter);
				int result2 = database1.update("delete from authorities where userName=:userName", parameter);
				
				
				if((result1 == 1)&&(result2 == 1)){
					System.out.println("user Deleted");
					return true;
				}else{
					System.out.println("user not Deleted");
					return false;
				}	
			}else{
				return false;		
			}
					
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
