package test.junitTests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import model.User;
import model.UserDao;


@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:configs/model-context.xml", 
		"classpath:configs/security-context.xml",
		"classpath:test/config/testData.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TestUser {
	
	@Autowired
	private UserDao userDao;
	
		
	
	@Test
	public void createTest(){
		
		User user = new User("xang", "xang@gefe", "mania", "ROLE_USER", true);
		
		assertTrue("User exists true", userDao.userExists(user.getUserName()));
		
		assertTrue("User deletion true", userDao.deleteUser(user.getUserName()));
		
		assertFalse("User exists true", userDao.userExists(user.getUserName()));
		
		assertTrue("User creation true", userDao.createUser(user));
		
		assertTrue("User exists true", userDao.userExists(user.getUserName()));
		
		
		
	}

	
	
	
	
}














