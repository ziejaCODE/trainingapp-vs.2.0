<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<script type="text/javascript">
	
	function onReady() {
		
		alert("Reloading");
		
		var millisecondsToWait = 1;
		setTimeout(function() {
			window.location.href = "/justReload";
		}, millisecondsToWait);		
	}
	$(document).ready(onReady);
	
</script>