<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


		<script type="text/javascript">
			
					
			var tableNu = 0;
			var MapOfExercesesNames  = new Map();			
			var userExercises = [];
			var weekDays = [];
			var weekDaysToChoose = [];
			
			
			function onReady(){
				
				setWeekDays();	
				setExerciseNames();
				
				tableNu++;
				presentExercise(tableNu);
				
				document.getElementById("saveAndExitTrainingButton").disabled = true;
				
			}
			
			
			
			
			// this function creates select tag populated with days of the week options
			// and sets sets apropriate day selected according to training
			function setWeekDays(){	
				
				var trainName = document.getElementById("weekDay");
				
				weekDays = JSON.parse(document.getElementById("weekNamesListReceived").innerHTML);
				
				var ListOfWeekDays = [ "Select day", "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
				
				weekDaysToChoose = ListOfWeekDays;
				
				for (var i = 0; i < weekDays.length; i++) {
					if(ListOfWeekDays.includes(weekDays[i]) && (weekDays[i] != "Select day")){
						//alert(weekDays[i] + " is there ");
						weekDaysToChoose.splice(weekDaysToChoose.indexOf(weekDays[i]),1);
					}
				}				
				
				for(var i = 0; i < ListOfWeekDays.length; i++){
					
					var elemSeleList = document.createElement("option");
					var name = ListOfWeekDays[i];
					//alert("week day is now " + name);
					if((training != null) && (name == training.weekDay) ){
						//alert("exercise is now " + exercise.nameOfExercise + " and name is " + name);
						elemSeleList.innerHTML = training.weekDay;
						elemSeleList.setAttribute("value", training.weekDay);
						elemSeleList.setAttribute("selected", "selected")
					}else{
						elemSeleList.innerHTML = name;
						elemSeleList.setAttribute("value", name);							
					}				
					trainName.appendChild(elemSeleList);				
				}
			}
			
			
			function setExerciseNames(){
				
				MapOfExercesesNames = JSON.parse(document.getElementById("exeNamesMapReceived").innerHTML);				
				var keys = Object.keys(MapOfExercesesNames);				
				var values = Object.values(MapOfExercesesNames);				
				defineVaraiblesFromMap(MapOfExercesesNames);
			}
			
			$(document).ready(onReady);			
			
			function onClick() {				
				tableNu++;
				presentExercise(tableNu);			
			}
		
			
			
			
			function saveAndExit() {
				
				addExercise();
				
				var training = passTraining();
		
				//alert("userExercises to  " + userExercises); // test
				
				training.trainingName = training.trainingName.trim();
				
				var userAddition = "_" + document.getElementById("userNameHeader").innerHTML;
				
				if(document.getElementById("userNameHeader").innerHTML != "public"){
					training.trainingName = training.trainingName + "_" + document.getElementById("userNameHeader").innerHTML;
				}
								
				var ListOfTrainingNames = document.getElementById("usersTrainingsReceived").innerHTML;
								
				if(!(ListOfTrainingNames.includes(training.trainingName))){
				
					//saveNewExerciseNameToDatabases();
					var myJSON1 = JSON.stringify(userExercises);
				
					$.ajax({
						type : 'POST',
						url : "${pageContext.request.contextPath}/getNewExercise",
						data : myJSON1,
						dataType : "json",
						mimeType : 'application/json'
					});
					
					
					var myJSON = JSON.stringify(training);		
			
					$.ajax({
						type : 'POST',
						url : "${pageContext.request.contextPath}/doCreate",
						contentType : 'application/json; charset=utf-8',
						data : myJSON,
						dataType : "json",
						mimeType : 'application/json'
			
					});				
					window.location.href = "${pageContext.request.contextPath}/justReload";	
					
				}else if(userAddition == training.trainingName){
					alert("Please provide a valid Training Name");				
				}else{
					alert("Training Name already exists. Please change Training Name ");					
					
				}
			
			}
			
			
			function saveNewExerciseNameToDatabases() {
				
				var exeName = document.getElementById("newExeName").value;
				
				//alert("userExercises = " + userExercises.length);
				
				if(exeName != ""){	
					
					var keys = Object.keys(MapOfExercesesNames);				
					var values = Object.values(MapOfExercesesNames);
					
					var num = keys.indexOf("USERS EXERCISES");					
					userExercises = values[num];					
					userExercises.push(exeName);					
					values[num] = userExercises;
					
					//alert("exercises.length " + numOfExercises);
								
					var headerList = document.getElementsByTagName("OPTGROUP");
					
					//alert("headerList.length " + headerList.length);
								
					for(var i = 0; i < headerList.length; i++){				
						
						if(headerList[i].label == "USERS EXERCISES"){
							//alert("this is it " + headerList[i].label);
							
							var header = headerList[i];
							var elemSeleList = document.createElement("option");										
							
							elemSeleList.innerHTML = exeName;
							elemSeleList.setAttribute("value", exeName);
							header.appendChild(elemSeleList);
						}
					}					
					
					document.getElementById("newExeName").value = "";					
				}
			}
			
			
			
			
			
			
			
			
		</script>



	<div class="wholeForm">

		<div class="createTrain  container">
	
			<div class="trainingHeader container">
				<div class="trainingTitle row">
					<h1 class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">Create your own Training</h1>
					<a id="userNameHeader" style="display: none"><c:out value="${user.userName}"></c:out></a>
				</div>
					
					
					
					
				<div class="row" >				
					<p id="updateHeader" style="display: block">						
						<input id="trainingName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-5"
							type="text" placeholder="Please enter training name"/> 						
						<select id="weekDay" class="form-control col-xs-offset-0 col-xs-8 col-sm-3" ></select>	
					</p>				
				</div>
		
		
		
		
		
				<p class="row" id="displayHeader" style="display: none">
					
					<a  id="trainingNameDisplayed"  class="col-xs-offset-1 col-xs-10 col-sm-6">			
						<c:out value="${training.trainingName}"></c:out>
					</a> 					
					<a id="weekDayDisplayed"	class="col-xs-offset-0 col-xs-6 col-md-3">
						<c:out value="${training.weekDay}"></c:out>
					</a>
					
				</p>
				
			</div>
		
		
		
		
		
		
			<div class="container">
				<div class="row" id="newExerciseNameInput">
					<input id="newExeName" class="form-control col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-6" type="text" placeholder="Please enter exercise name" />
					<button class="btnNewExeName btn col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-4" 
							onclick="saveNewExerciseNameToDatabases()">Add New Exercise Name
					</button>
				</div>
			</div>	
		
			<div id=presentTable class="container"></div>			
			<div id="presentTraining" class="container"></div>			
			
			<div class="container">
				<div class="row" id="d" style="display: block">
					<div class="editTraining col-xs-offset-0 col-xs-5" >
							<button class="editTrainingButton btn "
							onclick="addExercise()"	>View Training</button>
					</div>
					<div class="saveTraining col-xs-offset-0 col-xs-5"  >
							<button id="saveAndExitTrainingButton" class="saveTrainingButton  btn"
							 onclick="saveAndExit()">Save and Exit</button>
					</div>			
				</div>
				
							
				<div class="row" id="e" style="display: none">				
					<div  class="editTraining col-xs-offset-0 col-xs-5" >
							<button class="editTrainingButton btn "
							onclick="addExercise()"	>Edit Training</button>
					</div>
					<div class="saveTraining col-xs-offset-0 col-xs-5"  >
							<button id="saveAndExitTrainingButton" class="saveTrainingButton  btn"
								onclick="saveAndExit()">Save and Exit</button>
					</div>			
				</div>			
			</div>
			
		
			<div class="container">
				<div class="goBackLink row">
					<sec:authorize access="isAuthenticated()">
						<p class="goBack col-xs-offset-1 col-xs-10">
							<a href="<c:url value='/homePage'/>">Go back to home page</a>
						</p>
					</sec:authorize>
				</div>
			</div>
			
			
		<p id="usersTrainingsReceived" style="display: none"><c:out value="${userListOfTrainings}"></c:out></p>		
		<p id="exeNamesReceived" style="display: none"> <c:out value="${names}"></c:out></p>		
		<p id="exeNamesMapReceived" style="display: none"> <c:out value="${namesMap}"></c:out></p>		
		<p id="weekNamesListReceived" style="display: block"><c:out value="${weekDaysList}"></c:out></p>
		</div>
	</div>
	
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/scripts/changePos11.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/scripts/displayWorking.js"></script>





