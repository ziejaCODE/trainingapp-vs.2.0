<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="aboutPageHead container-fluid">
	<div class="row">
		<div class="col-xs-offset-0 col-xs-12 col-sm-offset-2 col-sm-8">
			<div class="aboutText">
				<h1>About Training App</h1>
				<p>The Online Training Organiser helps to build detailed,
					advanced and highly customised personal plan for bodybuilding and
					fitness training.</p>
			</div>
		</div>
	</div>
</div>
<div class="aboutPage container-fluid">
	<div class="row">
		<div
			class="aboutFeatures col-xs-offset-0 col-xs-12 col-sm-offset-2 col-sm-8">
			<h1 class="featuresTitle">Features</h1>
			<ul class="featuresList " >
				<li class="aboutlistItem list-group-item">
				<!--  <span class="glyphicon glyphicon-scissors" style="margin-right: 10px"></span>-->
				Create highly customised training plans up to personal preferences
					on mobile or desktop</li>
				<li class="aboutlistItem list-group-item">
				<!--  <span class="glyphicon glyphicon-phone" style="margin-right: 10px"></span>-->
					Quick access to your current training plan anywhere on your mobile</li>
				<li class="aboutlistItem list-group-item">
				<!--  <span class="glyphicon glyphicon-list" style="margin-right: 10px"></span>-->
					Try our ready to use training plans</li>
				<li class="aboutlistItem list-group-item">
				<!--  <span class="glyphicon glyphicon-circle-arrow-down" style="margin-right: 10px"></span>-->
					Use exercises database to create your training plan or add your own to your private repository</li>

			</ul>
		</div>
		<sec:authorize access="!isAuthenticated()">
			<div class="registerHere col-xs-offset-0 col-xs-12">
				<a  href="<c:url value='/createNewAccount'/>">Register now</a><br>
				<br>
			</div>
		</sec:authorize>
	</div>
</div>







