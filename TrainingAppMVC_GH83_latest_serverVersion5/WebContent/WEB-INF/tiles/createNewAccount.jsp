<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
			
			function onload(){				
				$("#password").keyup(comparePasswordEntry);
				$("#confirmpassword").keyup(comparePasswordEntry);
				$("#userInfo").submit(submitAllowed);
			}			
			function submitAllowed(){
				
				var password = $("#password").val();
				var confirmpassword = $("#confirmpassword").val();
				
				if(password != confirmpassword){
					return false;
				}else{
					return true;
				}				
			}
			
			function comparePasswordEntry(){				
				var password = $("#password").val();
				var confirmpassword = $("#confirmpassword").val();				
				
				if(password == confirmpassword){
					$("#passConfirm").text("<fmt:message key='CorrectPassword.user.password'/>");
					$("#passConfirm").addClass("valid");
					$("#passConfirm").removeClass("error");
				}else{
					$("#passConfirm").text("<fmt:message key='WrongPassword.user.password'/>");
					$("#passConfirm").addClass("error");
					$("#passConfirm").removeClass("valid");
				}				
			}
			
			$(document).ready(onload);			
			
</script>

<div class="logInFormContainer   container-fluid">
	<div class="createAccountFormBox">
		<div>
			<h1 class="text-center create-title">Create new user account</h1>
		</div>
		<div>
			<f:form id="userInfo" class="form-createAccount"
				action="${pageContext.request.contextPath}/doCreateAccount"
				method="post" commandName="user">

				<f:input name="userName" type="text" path="userName"
					placeholder="UserName" />
				<br />
				<f:errors path="userName" cssClass="error"></f:errors>
				<f:input name="userEmail" type="text" path="userEmail"
					placeholder="User Email" />
				<br />
				<f:errors path="userEmail" cssClass="error"></f:errors>
				<f:input id="password" name="password" type="password" path="password"
					placeholder="Password" />
				<br />
				<f:errors path="password" cssClass="error"></f:errors>
				<input id="confirmpassword" name="confirmpassword" type="password"
					placeholder="Confirm Password" />
				<div id="passConfirm"></div>


				<button class="btn btn-lg btn-primary btn-block" type="submit"
					value="Create account">Create account</button>
			</f:form>
		</div>

	</div>
</div>
